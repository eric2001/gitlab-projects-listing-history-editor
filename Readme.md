# GitLab Projects Listing History Editor 
![GitLab Projects Listing History Editor](./screenshot.png) 

## Description
This project was created to manage the history.xml file used by the [GitLab Projects Listing Page](https://gitlab.com/eric2001/gitlab-projects-listing-page) project.

**License:** [GPL v.3](http://www.gnu.org/copyleft/gpl.html)

## Installation
 - Extract the contents of the binary .zip file to a folder and run the GitLabHistoryEditor.exe file.  

## Instructions
Adding a new project
 - Select File -> Add Project to add a new project.  On the pop-up window fill in the GitLab Project ID and Project Name from the main page of the project, then press Save.

Editing an existing project:
 - Select the project you wish to edit the name of, then select File -> Edit Project.  Enter in a new project name and press save.
  
Adding a release to a project:
 - Select the project you wish to add a release to, then select File -> Add Release.  On the pop-up window fill in the release date and version number, then press Save.

After you've finished adding projects and releases, quit the program.  The updated history.xml file can be found in the Data folder.
 
## History
**Version 1.0.0:**
> - Initial Release
> - Released on 19 July 2021
>
> Download: [Version 1.0.0 Binary](/uploads/22fa57fc78e22815683a74c1195c1143/GitLabHistoryEditor100Binary.zip) | [Version 1.0.0 Source](/uploads/a336581d7c41bda81a79447fcbd39a15/GitLabHistoryEditor100Source.zip)
