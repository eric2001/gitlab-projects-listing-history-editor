﻿namespace GitLabHistoryEditor
{
    partial class AddProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddProject));
            this.lblProjectId = new System.Windows.Forms.Label();
            this.lblProjectName = new System.Windows.Forms.Label();
            this.textProjectId = new System.Windows.Forms.TextBox();
            this.textProjectName = new System.Windows.Forms.TextBox();
            this.buttonAddProject = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblProjectId
            // 
            this.lblProjectId.AutoSize = true;
            this.lblProjectId.Location = new System.Drawing.Point(12, 9);
            this.lblProjectId.Name = "lblProjectId";
            this.lblProjectId.Size = new System.Drawing.Size(91, 13);
            this.lblProjectId.TabIndex = 0;
            this.lblProjectId.Text = "GitLab Project ID:";
            // 
            // lblProjectName
            // 
            this.lblProjectName.AutoSize = true;
            this.lblProjectName.Location = new System.Drawing.Point(12, 35);
            this.lblProjectName.Name = "lblProjectName";
            this.lblProjectName.Size = new System.Drawing.Size(74, 13);
            this.lblProjectName.TabIndex = 1;
            this.lblProjectName.Text = "Project Name:";
            // 
            // textProjectId
            // 
            this.textProjectId.Location = new System.Drawing.Point(109, 6);
            this.textProjectId.Name = "textProjectId";
            this.textProjectId.Size = new System.Drawing.Size(268, 20);
            this.textProjectId.TabIndex = 1;
            // 
            // textProjectName
            // 
            this.textProjectName.Location = new System.Drawing.Point(109, 32);
            this.textProjectName.Name = "textProjectName";
            this.textProjectName.Size = new System.Drawing.Size(268, 20);
            this.textProjectName.TabIndex = 2;
            // 
            // buttonAddProject
            // 
            this.buttonAddProject.Location = new System.Drawing.Point(302, 58);
            this.buttonAddProject.Name = "buttonAddProject";
            this.buttonAddProject.Size = new System.Drawing.Size(75, 23);
            this.buttonAddProject.TabIndex = 3;
            this.buttonAddProject.Text = "Save";
            this.buttonAddProject.UseVisualStyleBackColor = true;
            this.buttonAddProject.Click += new System.EventHandler(this.buttonAddProject_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(221, 58);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // AddProject
            // 
            this.AcceptButton = this.buttonAddProject;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(389, 91);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonAddProject);
            this.Controls.Add(this.textProjectName);
            this.Controls.Add(this.textProjectId);
            this.Controls.Add(this.lblProjectName);
            this.Controls.Add(this.lblProjectId);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddProject";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add Project";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblProjectId;
        private System.Windows.Forms.Label lblProjectName;
        private System.Windows.Forms.TextBox textProjectId;
        private System.Windows.Forms.TextBox textProjectName;
        private System.Windows.Forms.Button buttonAddProject;
        private System.Windows.Forms.Button buttonCancel;
    }
}