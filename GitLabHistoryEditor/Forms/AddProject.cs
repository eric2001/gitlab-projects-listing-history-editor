﻿// Project Name:  GitLab History Editor
// Copyright 2021 Eric Cavaliere
// License:  GPL Version 3
// Project Start Date: 19 July 2021

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GitLabHistoryEditor
{
    public partial class AddProject : Form
    {

        public FormMain parent;

        public AddProject()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Close the Add Project window without saving anything.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAddProject_Click(object sender, EventArgs e)
        {
            if (textProjectId.Text == string.Empty)
            {
                MessageBox.Show("Please enter a Project Id.", "GitLab History Editor", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (textProjectName.Text == string.Empty)
            {
                MessageBox.Show("Please enter a Project Name.", "GitLab History Editor", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            parent.AddProject(Convert.ToInt32(textProjectId.Text), textProjectName.Text);
            this.Close();
        }
    }
}
