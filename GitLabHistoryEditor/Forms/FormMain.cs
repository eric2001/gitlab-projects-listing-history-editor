﻿// Project Name:  GitLab History Editor
// Copyright 2021 Eric Cavaliere
// License:  GPL Version 3
// Project Start Date: 19 July 2021

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GitLabHistoryEditor
{
    public partial class FormMain : Form
    {
        public XmlData records = new XmlData();

        public FormMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Display the About window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAbout about = new FormAbout();
            about.ShowDialog();
        }

        /// <summary>
        /// Load any previously saved data into the main window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMain_Load(object sender, EventArgs e)
        {
            LoadProjects();
        }

        /// <summary>
        /// Display all previously saved projects on the screen.
        /// </summary>
        private void LoadProjects()
        {
            foreach (Project row in records.Projects)
            {
                ListViewItem OneProject = new ListViewItem();
                OneProject.Text = row.ProjectName;
                OneProject.Tag = row.ProjectId;
                listProjects.Items.Add(OneProject);
            }
        }

        /// <summary>
        /// Add a new project to the data store and screen or update an existing one.
        /// </summary>
        /// <param name="ProjectId"></param>
        /// <param name="ProjectName"></param>
        public void AddProject(int ProjectId, string ProjectName)
        {
            if (records.Projects.Where(p => p.ProjectId == ProjectId).Count() == 0)
            {
                ListViewItem OneProject = new ListViewItem();
                OneProject.Text = ProjectName;
                OneProject.Tag = ProjectId;
                listProjects.Items.Add(OneProject);
            }
            else
            {
                listProjects.SelectedItems[0].Text = ProjectName;
            }
            records.AddProject(ProjectId, ProjectName);
        }

        /// <summary>
        /// Open the edit window to create a new project.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddProject NewProjectWindow = new AddProject();
            NewProjectWindow.parent = this;
            NewProjectWindow.ShowDialog();
        }

        /// <summary>
        /// Open the edit window to modify an existing project.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listProjects.SelectedItems.Count > 0)
            {
                AddProject NewProjectWindow = new AddProject();
                NewProjectWindow.Text = "Edit Project";
                ((TextBox)NewProjectWindow.Controls["textProjectId"]).ReadOnly = true;
                ((TextBox)NewProjectWindow.Controls["textProjectId"]).Text = listProjects.SelectedItems[0].Tag.ToString();
                ((TextBox)NewProjectWindow.Controls["textProjectName"]).Text = listProjects.SelectedItems[0].Text;
                NewProjectWindow.parent = this;
                NewProjectWindow.ShowDialog();
            }
            else
            {
                MessageBox.Show("Please select Project to edit from the list.", "GitLab History Editor", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Load a list of releases for the selected project.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listProjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            listHistory.Items.Clear();

            if (listProjects.SelectedItems.Count > 0)
            {
                foreach (History release in records.Releases.Where(r => r.ProjectId == Convert.ToInt32(listProjects.SelectedItems[0].Tag)).ToList())
                {
                    ListViewItem OneRelease = new ListViewItem();
                    OneRelease.Text = release.ReleaseDate;
                    OneRelease.SubItems.Add(release.VersionNumber);
                    OneRelease.Tag = release.HistoryId;
                    listHistory.Items.Add(OneRelease);
                }
            }
        }

        /// <summary>
        /// Exit the application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Open the edit window to create a new release for the selected project.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addReleaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listProjects.SelectedItems.Count > 0)
            {
                FormAddRelease AddRelease = new FormAddRelease();
                AddRelease.parent = this;
                AddRelease.Tag = listProjects.SelectedItems[0].Tag;
                ((TextBox)AddRelease.Controls["textProjectName"]).Text = listProjects.SelectedItems[0].Text;
                AddRelease.ShowDialog();
            }
            else
            {
                MessageBox.Show("Please select Project to add a release to from the list.", "GitLab History Editor", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Add a new release to the data store and screen.
        /// </summary>
        /// <param name="ProjectId">ID of the project to add a release to.</param>
        /// <param name="ProjectName">Name of the project to add a release to.</param>
        /// <param name="ReleaseDate">Date of this release.</param>
        /// <param name="VersionNumber">Version number for this release.</param>
        public void AddRelease(int ProjectId, string ProjectName, string ReleaseDate, string VersionNumber)
        {
            ListViewItem OneRelease = new ListViewItem();
            OneRelease.Text = ReleaseDate;
            OneRelease.SubItems.Add(VersionNumber);
            OneRelease.Tag = records.AddHistory(ProjectId, ProjectName, ReleaseDate, VersionNumber);
            listHistory.Items.Add(OneRelease);
        }
    }
}
