﻿namespace GitLabHistoryEditor
{
    partial class FormAddRelease
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAddRelease));
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.labelProjectName = new System.Windows.Forms.Label();
            this.labelReleaseDate = new System.Windows.Forms.Label();
            this.labelVersionNumber = new System.Windows.Forms.Label();
            this.textProjectName = new System.Windows.Forms.TextBox();
            this.textVersionNumber = new System.Windows.Forms.TextBox();
            this.datePickerReleaseDate = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(144, 84);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(225, 84);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 4;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // labelProjectName
            // 
            this.labelProjectName.AutoSize = true;
            this.labelProjectName.Location = new System.Drawing.Point(12, 9);
            this.labelProjectName.Name = "labelProjectName";
            this.labelProjectName.Size = new System.Drawing.Size(74, 13);
            this.labelProjectName.TabIndex = 2;
            this.labelProjectName.Text = "Project Name:";
            // 
            // labelReleaseDate
            // 
            this.labelReleaseDate.AutoSize = true;
            this.labelReleaseDate.Location = new System.Drawing.Point(12, 38);
            this.labelReleaseDate.Name = "labelReleaseDate";
            this.labelReleaseDate.Size = new System.Drawing.Size(75, 13);
            this.labelReleaseDate.TabIndex = 3;
            this.labelReleaseDate.Text = "Release Date:";
            // 
            // labelVersionNumber
            // 
            this.labelVersionNumber.AutoSize = true;
            this.labelVersionNumber.Location = new System.Drawing.Point(12, 61);
            this.labelVersionNumber.Name = "labelVersionNumber";
            this.labelVersionNumber.Size = new System.Drawing.Size(85, 13);
            this.labelVersionNumber.TabIndex = 4;
            this.labelVersionNumber.Text = "Version Number:";
            // 
            // textProjectName
            // 
            this.textProjectName.Location = new System.Drawing.Point(100, 6);
            this.textProjectName.Name = "textProjectName";
            this.textProjectName.ReadOnly = true;
            this.textProjectName.Size = new System.Drawing.Size(200, 20);
            this.textProjectName.TabIndex = 1;
            // 
            // textVersionNumber
            // 
            this.textVersionNumber.Location = new System.Drawing.Point(100, 58);
            this.textVersionNumber.Name = "textVersionNumber";
            this.textVersionNumber.Size = new System.Drawing.Size(200, 20);
            this.textVersionNumber.TabIndex = 3;
            // 
            // datePickerReleaseDate
            // 
            this.datePickerReleaseDate.Location = new System.Drawing.Point(100, 32);
            this.datePickerReleaseDate.Name = "datePickerReleaseDate";
            this.datePickerReleaseDate.Size = new System.Drawing.Size(200, 20);
            this.datePickerReleaseDate.TabIndex = 2;
            // 
            // FormAddRelease
            // 
            this.AcceptButton = this.buttonSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(307, 113);
            this.Controls.Add(this.datePickerReleaseDate);
            this.Controls.Add(this.textVersionNumber);
            this.Controls.Add(this.textProjectName);
            this.Controls.Add(this.labelVersionNumber);
            this.Controls.Add(this.labelReleaseDate);
            this.Controls.Add(this.labelProjectName);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormAddRelease";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add Release";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label labelProjectName;
        private System.Windows.Forms.Label labelReleaseDate;
        private System.Windows.Forms.Label labelVersionNumber;
        private System.Windows.Forms.TextBox textProjectName;
        private System.Windows.Forms.TextBox textVersionNumber;
        private System.Windows.Forms.DateTimePicker datePickerReleaseDate;
    }
}