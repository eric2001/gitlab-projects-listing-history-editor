﻿// Project Name:  GitLab History Editor
// Copyright 2021 Eric Cavaliere
// License:  GPL Version 3
// Project Start Date: 19 July 2021

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GitLabHistoryEditor
{
    public enum Tables
    {

        Project,
        History
    }

    public class XmlData
    {

        /// <summary>
        /// Contains all data in the history.xml file.
        /// </summary>
        private DataSet _Data = new DataSet("History");

        /// <summary>
        /// Contains the path and file name for the history.xml file.
        /// </summary>
        private string _XmlFile = string.Empty;

        /// <summary>
        /// Contains the path and file name to the schema for the history.xml file.
        /// </summary>
        private string _XmlSchema = string.Empty;

        /// <summary>
        /// Contains a list of all existing projects from the history.xml file.
        /// </summary>
        private List<Project> _Projects;

        /// <summary>
        /// Contains a list of all existing projects from the history.xml file.
        /// </summary>
        public List<Project> Projects
        {
            get
            {
                if (this._Projects == null)
                {
                    // Load all existing projects from the xml file.
                    this._Projects = new List<Project>();
                    this.LoadProjects();
                }

                return this._Projects;
            }
        }

        /// <summary>
        /// Contains a list of all existing releases from the history.xml file.
        /// </summary>
        private List<History> _Releases;

        /// <summary>
        /// Contains a list of all existing releases from the history.xml file.
        /// </summary>
        public List<History> Releases
        {
            get
            {
                if (this._Releases == null)
                {
                    // Load all existing releases from the xml file.
                    this._Releases = new List<History>();
                    this.LoadReleases();
                }

                return this._Releases;
            }
        }

        /// <summary>
        /// Create a new instance of the XmlData Class.
        /// </summary>
        public XmlData()
        {

            // Set up the file names.
            this._XmlFile = Application.StartupPath + @"\Data\history.xml";
            this._XmlSchema = Application.StartupPath + @"\Data\history.xsd";

            // Load all saved data from the Budget.xml file.
            this.LoadData();

        }

        /// <summary>
        /// Loads all previously saved data from the history.xml file.
        /// </summary>
        private void LoadData()
        {
            // Load all saved data from the .xml file.
            this._Data.ReadXmlSchema(this._XmlSchema);
            this._Data.ReadXml(this._XmlFile);
        }

        /// <summary>
        /// Load a list of existing projects from the history.xml file.
        /// </summary>
        private void LoadProjects()
        {

            // Load each project record from the xml file into an in memory list for easier access.
            foreach (DataRow row in this._Data.Tables[Tables.Project.ToString()].Rows)
            {
                Project NewProject = new Project();
                NewProject.ProjectId = Convert.ToInt32(row["Project_Id"]);
                NewProject.ProjectName = row["Project_Name"].ToString();
                this._Projects.Add(NewProject);
            }
        }

        /// <summary>
        /// Add a new project to the XML data or update an existing project.
        /// </summary>
        /// <param name="ProjectId">ID of the new project.</param>
        /// <param name="ProjectName">Name of the new project</param>
        public void AddProject(int ProjectId, string ProjectName)
        {
            if (this._Projects.Where(p => p.ProjectId == ProjectId).Count() == 0)
            {
                Project NewProject = new Project();
                NewProject.ProjectId = ProjectId;
                NewProject.ProjectName = ProjectName;
                this._Projects.Add(NewProject);
            }
            else
            {
                this._Projects.Where(p => p.ProjectId == ProjectId).FirstOrDefault().ProjectName = ProjectName;
            }

            SaveData();
        }

        /// <summary>
        /// Add a new release to an existing project.
        /// </summary>
        /// <param name="ProjectId">ID of the new project.</param>
        /// <param name="ProjectName">Name of the new project</param>
        /// <param name="ReleaseDate">Release Date to display</param>
        /// <param name="VersionNumber">Version Number of the release for this date</param>
        public int AddHistory(int ProjectId, string ProjectName, string ReleaseDate, string VersionNumber)
        {
            History NewRelease = new History();
            NewRelease.ProjectId = ProjectId;
            NewRelease.ProjectName = ProjectName;
            NewRelease.ReleaseDate = ReleaseDate;
            NewRelease.VersionNumber = VersionNumber;
            if (this._Releases.Count == 0)
            {
                NewRelease.HistoryId = 1;
            }
            else
            {
                NewRelease.HistoryId = this._Releases.OrderByDescending(r => r.HistoryId).FirstOrDefault().HistoryId + 1;
            }

            this._Releases.Add(NewRelease);
            SaveData();

            return NewRelease.HistoryId;
        }

        /// <summary>
        /// Load a list of existing releases from the history.xml file.
        /// </summary>
        private void LoadReleases()
        {

            // Load each release record from the xml file into an in memory list for easier access.
            foreach (DataRow row in this._Data.Tables[Tables.History.ToString()].Rows)
            {
                History NewHistory = new History();
                NewHistory.HistoryId = Convert.ToInt32(row["History_Id"]);
                NewHistory.ProjectId = Convert.ToInt32(row["Project_Id"]);
                NewHistory.ProjectName = row["Project_Name"].ToString();
                NewHistory.ReleaseDate = row["Release_Date"].ToString();
                NewHistory.VersionNumber = row["Version_Number"].ToString();
                this._Releases.Add(NewHistory);
            }
        }

        /// <summary>
        /// Save all data to disk.
        /// </summary>
        public void SaveData()
        {
            // Update xml in memory with any recent changes.
            UpdateProjectData();
            UpdateReleaseData();

            // Save everything to disk.
            this._Data.WriteXml(this._XmlFile);
        }

        /// <summary>
        /// Update the Project data in the in memory copy of the xml file with the current data from the Projects list.
        /// </summary>
        public void UpdateProjectData()
        {

            // Loop through and create / update all the projects
            foreach (Project row in this._Projects)
            {

                // Attempt to load the existing record for this project.
                DataRow[] ExistingProject = this._Data.Tables[Tables.Project.ToString()].Select(string.Format("Project_Id = {0}", row.ProjectId));

                if (ExistingProject.Count() > 0)
                {
                    // If this project is already in _Data, then update it with the current values.
                    ExistingProject[0]["Project_Name"] = row.ProjectName;

                    // Update history records with name change.
                    foreach (History releaseRow in this._Releases.Where(r => r.ProjectId == row.ProjectId).ToList())
                    {
                        releaseRow.ProjectName = row.ProjectName;
                    }
                }
                else
                {
                    // If this Project was not already in _Data, then create a new record
                    DataRow NewRow = this._Data.Tables[Tables.Project.ToString()].NewRow();
                    NewRow["Project_Id"] = row.ProjectId;
                    NewRow["Project_Name"] = row.ProjectName;

                    // Add the new row to the Projects table in _Data
                    this._Data.Tables[Tables.Project.ToString()].Rows.Add(NewRow);
                }
            }
        }

        /// <summary>
        /// Update the releases data in the in memory copy of the xml file with the current data from the Releases list.
        /// </summary>
        public void UpdateReleaseData()
        {

            // Loop through and create / update all the releases
            if (this._Releases != null)
            {
                foreach (History row in this._Releases)
                {

                    // Attempt to load the existing record for this release.
                    DataRow[] ExistingRelease = this._Data.Tables[Tables.History.ToString()].Select(string.Format("History_Id = {0}", row.HistoryId));

                    if (ExistingRelease.Count() > 0)
                    {
                        // If this release is already in _Data, then update it with the current values.
                        ExistingRelease[0]["Project_Id"] = row.ProjectId;
                        ExistingRelease[0]["Project_Name"] = row.ProjectName;
                        ExistingRelease[0]["Release_Date"] = row.ReleaseDate;
                        ExistingRelease[0]["Version_Number"] = row.VersionNumber;
                    }
                    else
                    {
                        // If this release was not already in _Data, then create a new record
                        DataRow NewRow = this._Data.Tables[Tables.History.ToString()].NewRow();
                        NewRow["History_Id"] = row.HistoryId;
                        NewRow["Project_Id"] = row.ProjectId;
                        NewRow["Project_Name"] = row.ProjectName;
                        NewRow["Release_Date"] = row.ReleaseDate;
                        NewRow["Version_Number"] = row.VersionNumber;

                        // Add the new row to the History table in _Data
                        this._Data.Tables[Tables.History.ToString()].Rows.Add(NewRow);
                    }
                }
            }
        }
    }
}
